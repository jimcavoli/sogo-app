# SOGo Cloudron App

This repository contains the Cloudron app package source for [SOGo](http://sogo.nu/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=nu.sogo.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id nu.sogo.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd sogo-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, backup and restore.

```
cd sogo-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

## Notes

We cannot add support for multiple mailboxes per Cloudron user, because SOGo has no concept of pulling mailboxes per user via ldap.
We decided it is just an Email client and the user logs in explicitly with the mailbox he wants to use.
Adding more IMAP mailboxes is disabled because it is confusing, if the other mailbox is also on the Cloudron. Sogo will create separate users for both email addresses if you login with them, regardless of already added in another session.
