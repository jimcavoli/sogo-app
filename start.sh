#!/bin/bash

set -eu -o pipefail

echo
echo "##########################"
echo "##     SOGo startup     ##"
echo "##########################"
echo

echo "=> Create sogo.conf"
cat <<EOF > /run/sogo.conf
{
    SOGoProfileURL = "${CLOUDRON_MYSQL_URL}/sogo_user_profile";
    OCSFolderInfoURL = "${CLOUDRON_MYSQL_URL}/sogo_folder_info";
    OCSSessionsFolderURL = "${CLOUDRON_MYSQL_URL}/sogo_sessions_folder";
    OCSEMailAlarmsFolderURL = "${CLOUDRON_MYSQL_URL}/sogo_alarms_folder";
    SOGoLanguage = English;
    SOGoAppointmentSendEMailNotifications = YES;
    SOGoFoldersSendEMailNotifications = NO;
    SOGoACLsSendEMailNotifications = NO;
    SOGoNotifyOnPersonalModifications = NO;
    SOGoNotifyOnExternalModifications = NO;
    SOGoTimeZone = UTC;
    SOGoVacationEnabled = YES;
    SOGoForwardEnabled = YES;
    SOGoFirstDayOfWeek = 0;
    WOPort = "0.0.0.0:4000";
    WONoDetach = YES;
    WOPidFile = "/run/sogo.pid";
    WOWorkersCount = 5;
    WOUseRelativeURLs = YES;
    SOGoMailAuxiliaryUserAccountsEnabled = YES;
    SOGoLoginModule = "Mail";
    SOGoMemcachedHost = "127.0.0.1:11211";
    SOGoGravatarEnabled = YES;
    SOGoMailCustomFromEnabled = NO;
    SOGoEnableDomainBasedUID = NO;
    SOGoJunkFolderName = "Spam";

    /* If login fails 5 times in 10 seconds, block for 300 seconds */
    SOGoMaximumFailedLoginCount = 5;
    SOGoMaximumFailedLoginInterval = 10;
    SOGoFailedLoginBlockInterval = 300;

    /* Debug */
    SOGoDebugRequests = YES;
    SoDebugBaseURL = YES;
    ImapDebugEnabled = YES;
    LDAPDebugEnabled = YES;
    PGDebugEnabled = NO;
    MySQL4DebugEnabled = NO;
    SOGoUIxDebugEnabled = YES;
    WODontZipResponse = NO;
    WOLogFile = "/run/sogo.log";

    SOGoIMAPServer = "imaps://${CLOUDRON_MAIL_IMAP_SERVER}:${CLOUDRON_MAIL_IMAP_PORT}";
    SOGoSMTPServer = ${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTP_PORT};
    SOGoSieveServer = "sieve://${CLOUDRON_MAIL_SIEVE_SERVER}:${CLOUDRON_MAIL_SIEVE_PORT}/?tls=YES";
    SOGoMailingMechanism = smtp;
    SOGoSMTPAuthenticationType = PLAIN;
    SOGoSieveScriptsEnabled = YES;
    SOGoSieveFolderEncoding = UTF-8;
    SOGoUserSources = ({
        type = ldap;
        CNFieldName = displayname;
        IDFieldName = cn;
        UIDFieldName = cn;
        IMAPLoginFieldName = mail;
        baseDN = "${CLOUDRON_LDAP_MAILBOXES_BASE_DN}";
        bindDN = "${CLOUDRON_LDAP_BIND_DN}";
        bindPassword = "${CLOUDRON_LDAP_BIND_PASSWORD}";
        MailFieldNames = (mail,mail0,mail1,mail2,mail3,mail4,mail5,mail6,mail7,mail8,mail9);
        canAuthenticate = YES;
        hostname = ${CLOUDRON_LDAP_URL};
        id = cloudron_ldap;
        isAddressBook = NO;
    });
}
EOF

echo "=> Generating nginx.conf"
sed -e "s,##HOSTNAME##,${CLOUDRON_APP_DOMAIN}," \
    /app/code/nginx.conf  > /run/nginx.conf

echo "=> Ensure directories"
mkdir -p /run/GNUstep /run/nginx

echo "=> Make cloudron own the data"
mkdir -p /app/data/spool
chown -R cloudron:cloudron /run /app/data

tail -F /run/sogo.log &

echo "=> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i SOGo
